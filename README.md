# README #

### What is this repository for? ###

* SpriteGenerator generates png files representing a grid of multiple image files. Capable of laying out images in a two-dimensional format, based on yaml file config. Capable of handle images of different sizes using output anchors.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Dependencies
    * pip install PyYAML
    * pip install Pillow
* Parameters
    * --input
        * Name of yaml file used to describe image configuration
    * --outupt
        * Name of output image generated
    * --anchor (optional)
        * Anchor direction for different sized images: TOP_LEFT, CENTER, MID_RIGHT, BOTTOM_MID, etc