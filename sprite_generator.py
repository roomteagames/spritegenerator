#!/usr/local/bin/python3

import argparse
import yaml

from sprite_generator import image_reading
from sprite_generator import image_writing

def main():

    #Parse arguments
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-i', '--input', type=str, help='Input yaml file', required=True)
    parser.add_argument('-o', '--output', type=str, help='Output png file', required=True)
    parser.add_argument('-a', '--anchor', type=str, help='Anchor type', default='CENTER')

    args = parser.parse_args()
    input = args.input
    output = args.output
    anchor = args.anchor

    #Read input data structure
    input_config = {}
    with open(input, 'r') as stream:
        try:
            input_config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    image_analysis = {}
    try:
        
        #Read images
        image_analysis = image_reading.read_image_config(input_config)

        #Write image based on calculated offsets
        image_writing.write_image(image_analysis, output, anchor)

        print(f'INFO: Image generation complete. Wrote to \"{output}\"')

    finally:
        #Close all images that were read
        if 'data' in image_analysis:
            for row in image_analysis['data']:
                for frame in row:
                    if 'image' in frame:
                        frame['image'].close()



if __name__ == '__main__':
    main()