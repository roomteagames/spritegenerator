from enum import Enum

class AnchorType(Enum):
    TOP_LEFT = 0
    TOP_MID = 1
    TOP_RIGHT = 2

    MID_LEFT = 3
    CENTER = 4
    MID_RIGHT = 5

    BOTTOM_LEFT = 6
    BOTTOM_MID = 7
    BOTTOM_RIGHT = 8


def calc_frame_offsets(img_width, img_height, frame_width, frame_height, anchor_type):

    anchor = AnchorType[anchor_type]

    offset = (0, 0)
    width_diff = frame_width - img_width
    height_diff = frame_height - img_height

    #Calculate offset based on anchor type
    if anchor == AnchorType.TOP_LEFT:
        offset = (0, 0)

    elif anchor == AnchorType.TOP_MID:
        offset = (width_diff // 2, 0)

    elif anchor == AnchorType.TOP_RIGHT:
        offset = (width_diff, 0)

    elif anchor == AnchorType.MID_LEFT:
        offset = (0, height_diff // 2)

    elif anchor == AnchorType.CENTER:
        offset = (width_diff // 2, height_diff // 2)

    elif anchor == AnchorType.MID_RIGHT:
        offset = (width_diff, height_diff // 2)

    elif anchor == AnchorType.BOTTOM_LEFT:
        offset = (0, height_diff)

    elif anchor == AnchorType.BOTTOM_MID:
        offset = (width_diff // 2, height_diff)

    elif anchor == AnchorType.BOTTOM_RIGHT:
        offset = (width_diff, height_diff)

    return offset