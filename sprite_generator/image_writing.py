from PIL import Image
from sprite_generator import frame_offsets

def write_image_frame(output_image, frame, frame_width, frame_height, row_index, col_index, anchor_type):

    #Get frame properties
    frame_image = frame['image']
    frame_image_width = frame['width']
    frame_image_height = frame['height']

    #Calc paste position for frame
    frame_x = frame_width * col_index
    frame_y = frame_height * row_index
    width_offset, height_offset = frame_offsets.calc_frame_offsets(frame_image_width, frame_image_height, frame_width, frame_height, anchor_type)

    output_image.paste(frame_image, (frame_x + width_offset, frame_y + height_offset))


def write_image(config, output_name, anchor_type):

    rows = config['data']
    frame_width = config['frame_width']
    frame_height = config['frame_height']
    image_width = config['width']
    image_height = config['height']

    output_image = Image.new('RGBA', (image_width, image_height))

    #Paste each input image into output image
    if rows != None:
        row_index = 0
        for row in rows:
            
            col_index = 0
            for col in row:
                
                #Write single input image to output image
                if col['is_valid']:
                    write_image_frame(output_image, col, frame_width, frame_height, row_index, col_index, anchor_type)

                col_index += 1

            row_index += 1

    output_image.save(output_name)
    output_image.close()