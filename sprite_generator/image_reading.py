from os import path
from PIL import Image

def read_image(image_name):
    frame_analysis = {}
    
    width = 0
    height = 0
    is_valid = False

    #Attempt to open file as image
    try:
        frame_image = Image.open(image_name)
        width, height = frame_image.size

        frame_analysis['image'] = frame_image
        is_valid = True
    except IOError as exc:
        print(exc)

    #Set dict properties to be returned
    frame_analysis['is_valid'] = is_valid
    frame_analysis['width'] = width
    frame_analysis['height'] = height

    return frame_analysis


def read_image_config(config):
    prefix = config['prefix']
    files = config['files']
    
    max_width = 0
    max_height = 0
    max_cols = 0
    analysis = []

    #Loop through each outer array and inner array, representing each row for image, and each image in row
    for frame_files in files:
        row_analysis = []

        #Store largest number of columns founds
        if len(frame_files) > max_cols:
            max_cols = len(frame_files)

        #Loop through each column in row
        for frame in frame_files:
            full_name = f'{prefix}/{frame}'
            frame_analysis = read_image(full_name)
            row_analysis.append(frame_analysis)

            if frame_analysis['width'] > max_width:
                max_width = frame_analysis['width']

            if frame_analysis['height'] > max_height:
                max_height = frame_analysis['height']

        analysis.append(row_analysis)

    return {'width': (max_cols * max_width), 'height': (len(files) * max_height), 'frame_height': max_height, 'frame_width': max_width, 'data': analysis }

        
            

